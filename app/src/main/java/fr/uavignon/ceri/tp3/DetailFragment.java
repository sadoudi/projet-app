package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textItemName, textDescription, textTemperature, textHumidity, textCloudiness, textWind;
    private ImageView imgWeather;
    private ProgressBar progress;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String itemID = args.getItemNum();
        viewModel.setItem(itemID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textItemName = getView().findViewById(R.id.nameItem);
        textDescription = getView().findViewById(R.id.description);
        textTemperature = getView().findViewById(R.id.editTemperature);
        textHumidity = getView().findViewById(R.id.editHumidity);
        textCloudiness = getView().findViewById(R.id.editCloudiness);
        textWind = getView().findViewById(R.id.editWind);

        imgWeather = getView().findViewById(R.id.icon);

        progress = getView().findViewById(R.id.progress);


    }

    private void observerSetup() {
        viewModel.getItem().observe(getViewLifecycleOwner(),
                item -> {
                    if (item != null) {

                        textItemName.setText(item.getName());
                        textDescription.setText(item.getDescription());
                        if(item.getWorking()==true){
                            textTemperature.setText("Marche Encore");
                        }
                        else{
                            textTemperature.setText("Marche Plus");
                        }
                        if(item.getYear()==-1 || item.getYear()==0){
                            textHumidity.setText("Inconnu");
                        }
                        else{
                            textHumidity.setText(String.valueOf(item.getYear()));
                        }
                        textWind.setText(item.getBrand());
                    }
                });

    viewModel.getIsLoading().observe(getViewLifecycleOwner(),
            isLoading ->{
                if(isLoading){
                    progress.setVisibility(View.VISIBLE);
                }
                else{
                    progress.setVisibility(View.GONE);
                }
            }
            );
    viewModel.getWebServiceThrowable().observe(getViewLifecycleOwner(),
                throwable ->{
                    Snackbar snackbar = Snackbar
                            .make(getView(), throwable.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
        );
    }


}