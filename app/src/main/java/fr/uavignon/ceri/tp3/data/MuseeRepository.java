package fr.uavignon.ceri.tp3.data;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.ItemDao;
import fr.uavignon.ceri.tp3.data.database.ItemRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.ItemResponse;
import fr.uavignon.ceri.tp3.data.webservice.APIInterface;
import fr.uavignon.ceri.tp3.data.webservice.ItemResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.ItemRoomDatabase.databaseWriteExecutor;

public class MuseeRepository {

    private static final String TAG = MuseeRepository.class.getSimpleName();

    private MutableLiveData<Item> selectedItem=new MutableLiveData<>();;

    private final APIInterface api;

    public MutableLiveData<Boolean> isLoading=new MutableLiveData<Boolean>();
    public MutableLiveData<Throwable> webServiceThrowable=new MutableLiveData<>();

    private ItemDao itemDao;


    volatile int nbAPILoads=0;


    private MutableLiveData<ArrayList<Item>> items=new MutableLiveData<>();

    public MutableLiveData<ArrayList<Item>> getItems() {
        return items;
    }

    private static volatile MuseeRepository INSTANCE;

    public synchronized static MuseeRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new MuseeRepository(application);
        }

        return INSTANCE;
    }

    public MuseeRepository(Application application) {
        ItemRoomDatabase db = ItemRoomDatabase.getDatabase(application);
        itemDao = db.itemDao();

        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(APIInterface.class);
    }

    public MutableLiveData<Item> getSelectedItem() {
        return selectedItem;
    }


    public void removeAll(){
        itemDao.deleteAll();
        items.postValue(null);
    }


    public void loadItemsFromDatabase(){
        ArrayList<Item> allItems= (ArrayList<Item>) itemDao.getSynchrAllItems();
        items.postValue(allItems);
    }

    public void loadItems(){
        isLoading.postValue(Boolean.TRUE);

        api.getCollection().enqueue(
                new Callback<Map<String, ItemResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, ItemResponse>> call,
                                           Response<Map<String, ItemResponse>> response) {

                        ArrayList<Item> itemTMP=new ArrayList<>();
                        int i=0;
                        for (String key: response.body().keySet()) {
                            ItemResult.transferInfo(response.body().get(key), key, itemTMP);
                            System.out.println(itemTMP.get(i).getWorking());
                            long res=insertItem(itemTMP.get(i));
                            i=i+1;
                        }

                        items.setValue(itemTMP);

                        isLoading.postValue(Boolean.FALSE);

                    }

                    @Override
                    public void onFailure(Call<Map<String, ItemResponse>> call, Throwable t) {
                        if (nbAPILoads<=1){
                            isLoading.postValue(Boolean.FALSE);
                        }
                        else{
                            nbAPILoads=nbAPILoads-1;
                        }
                        webServiceThrowable.postValue(t);
                    }
                });

    }


    public long insertItem(Item newItem) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return itemDao.insert(newItem);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }

    public int updateCity(Item item) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return itemDao.update(item);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedItem.setValue(item);
        return res;
    }

    public void deleteCity(String id) {
        databaseWriteExecutor.execute(() -> {
            itemDao.deleteItem(id);
        });
    }

    public void getItem(String id)  {

        Future<Item> fitem = databaseWriteExecutor.submit(() -> {
            return itemDao.getItemById(id);
        });
        try {
            selectedItem.setValue(fitem.get());

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
