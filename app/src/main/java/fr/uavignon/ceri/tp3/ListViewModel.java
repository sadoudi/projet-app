package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import fr.uavignon.ceri.tp3.data.MuseeRepository;
import fr.uavignon.ceri.tp3.data.Item;

public class ListViewModel extends AndroidViewModel {
    private MuseeRepository repository;
    private MutableLiveData<Boolean> isLoading;

    private MutableLiveData<ArrayList<Item>> allItems;

    public ListViewModel (Application application) {
        super(application);
        allItems=new MutableLiveData<>();
        repository = MuseeRepository.get(application);
        isLoading=repository.isLoading;

        allItems=repository.getItems();
    }


    public void deleteCity(String id) {
        repository.deleteCity(id);
    }

    LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    LiveData<ArrayList<Item>> getAllItems() {
        return allItems;
    }

    public void loadItems(){
        Thread t = new Thread(){
            public void run(){
                repository.loadItems();

            }
        };
        t.start();

    }

    public void loadItemsFromDatabase(){
        Thread t = new Thread(){
            public void run(){
                repository.loadItemsFromDatabase();

            }
        };
        t.start();

    }
    public void removeAll(){
        Thread t = new Thread(){
            public void run(){
                repository.removeAll();

            }
        };
        t.start();

    }

}
