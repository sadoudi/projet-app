package fr.uavignon.ceri.tp3.data.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Item;

@Dao
public interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Item item);

    @Query("DELETE FROM item_database")
    void deleteAll();

    @Query("SELECT * from item_database ORDER BY name ASC")
    List<Item> getSynchrAllItems();

    @Query("DELETE FROM item_database WHERE _id = :id")
    void deleteItem(String id);

    @Query("SELECT * FROM item_database WHERE _id = :id")
    Item getItemById(String id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Item item);
}
