package fr.uavignon.ceri.tp3.data.webservice;

import java.util.ArrayList;

import fr.uavignon.ceri.tp3.data.Item;

public class ItemResult {


    public static void transferInfo(ItemResponse body, String key,ArrayList<Item> items) {

        Item item = new Item();
        item.setId(key);
        item.setName(body.name);
        item.setDescription(body.description);
        item.setWorking(body.working);
        if (body.year!=null) item.setYear(body.year);

        if (body.brand!=null) item.setBrand(body.brand);

        items.add(item);
    }

}
