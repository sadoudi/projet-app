package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.MuseeRepository;
import fr.uavignon.ceri.tp3.data.Item;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();
    private MutableLiveData<Boolean> isLoading;
    private MuseeRepository repository;
    private MutableLiveData<Item> item;
    private  MutableLiveData<Throwable> webServiceThrowable;


    public DetailViewModel (Application application) {
        super(application);
        isLoading=new MutableLiveData<Boolean>();
        repository = MuseeRepository.get(application);
        isLoading=repository.isLoading;
        webServiceThrowable=repository.webServiceThrowable;
        item = new MutableLiveData<>();
    }

    public void setItem(String id) {
        repository.getItem(id);
        item = repository.getSelectedItem();
    }

    LiveData<Item> getItem() {
        return item;
    }

    LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    LiveData<Throwable> getWebServiceThrowable(){return webServiceThrowable;}


}

