package fr.uavignon.ceri.tp3.data;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.List;

@Entity(tableName = "item_database", indices = {@Index(value = {"name", "description"},
        unique = true)})
public class Item {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name="_id")
    private String id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @NonNull
    @ColumnInfo(name="description")
    private String description=null;

    @NonNull
    @ColumnInfo(name="brand")
    private String brand="Inconnu";

    @NonNull
    @ColumnInfo(name="working")
    private boolean working=false;

    @NonNull
    @ColumnInfo(name="year")
    private int year=-1;


    public Item() {

    }


    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getWorking() {
        return working;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public void setWorking(Boolean working) {
        this.working = working;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand=brand;
    }

    @NonNull
    public String getBrand() {
        return brand;
    }
}
